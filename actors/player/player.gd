extends Area2D
signal hit

export (int) var movement_speed = 400
var screen_size # game window screen size


func _ready():
    hide()
    screen_size = get_viewport_rect().size


func _process(delta):
    var velocity = Vector2() # The player's movement vector.
    
    # Affect velocity based on input
    if Input.is_action_pressed("ui_right"):
        velocity.x +=1
    if Input.is_action_pressed("ui_left"):
        velocity.x -=1
    if Input.is_action_pressed("ui_down"):
        velocity.y +=1
    if Input.is_action_pressed("ui_up"):
        velocity.y -=1

    if velocity.length() > 0:
        # set correct speed and start animation
        velocity = velocity.normalized() * movement_speed
        $AnimatedSprite.play()
    else:
        $AnimatedSprite.stop()

    # move player
    position += velocity * delta
    # make player stay within screen bounds
    position.x = clamp(position.x, 0, screen_size.x)
    position.y = clamp(position.y, 0, screen_size.y)

    # horizontal animation
    if velocity.x != 0:
        $AnimatedSprite.animation = "right"
        # vertical animation
    elif velocity.y != 0:
        $AnimatedSprite.animation = "up"

    $AnimatedSprite.flip_h = velocity.x < 0 # flip if moving left
    $AnimatedSprite.flip_v = velocity.y > 0 # flip if moving down


# when player is hit, game is over
func _on_Player_body_entered(body):
    die()
    emit_signal("hit")


# spawn player
func spawn(pos):
    position = pos
    show()
    $CollisionShape2D.disabled = false


# kill player
func die():
    hide()
    $CollisionShape2D.disabled = true
