extends RigidBody2D

export (int) var min_speed = 150
export (int) var max_speed = 250
var enemy_types = ["1", "2", "3"]


func _ready():
	# choose a random enemy type for the animation
    $AnimatedSprite.animation = enemy_types[randi() % enemy_types.size()]


# delete enemy when out of screen
func _on_Visibility_screen_exited():
    queue_free()
