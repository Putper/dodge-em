extends Node

export (PackedScene) var Enemy
var score


func _ready():
    randomize()


func new_game():
    score = 0
    $Player.spawn($StartPosition.position)
    $StartTimer.start()
    $HUD.update_score(score)
    $HUD.show_message("Get Ready")
    $Music.play()
    

func game_over():
    $Music.stop()
    $DeathSound.play()
    $ScoreTimer.stop()
    $EnemyTimer.stop()
    $HUD.show_game_over()


func _on_StartTimer_timeout():
    $EnemyTimer.start()
    $ScoreTimer.start()


func _on_ScoreTimer_timeout():
    score += 1
    $HUD.update_score(score)    


func _on_EnemyTimer_timeout():
    # random location on Path2D
    $EnemyPath/EnemySpawnLocation.set_offset(randi())
    # add new enemy to scene
    var enemy = Enemy.instance()
    add_child(enemy)
    # Set enemy's direction perpendicular to path direction
    var direction = $EnemyPath/EnemySpawnLocation.rotation + PI / 2
    # Set enemy's position to random location
    enemy.position = $EnemyPath/EnemySpawnLocation.position
    # make direction more random
    direction += rand_range(-PI / 4, PI / 4)
    enemy.rotation = direction
    # Choose random velocity
    enemy.set_linear_velocity(
        Vector2( rand_range(enemy.min_speed, enemy.max_speed), 0 ).rotated(direction)
    )
